*! version 1.0.0  04nov2022 R. Sesma

/*
Test user-defined commands
*/

program define test_commands
	version 12
	syntax [anything], dir(string) dofile(string)
	
	cd "`dir'"
	foreach j of numlist 0 1 {
		log using t`j'.txt, replace text
		local i = cond(`j'==0,"0","")
		do `dofile' `i'
		log close
	}
end
